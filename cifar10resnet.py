import torch
import torch.nn as nn
import torch.optim as optim
import torch.optim.lr_scheduler as lr_scheduler
from torch.utils.data import DataLoader
from torch.utils.data import sampler
import torchvision.datasets as datasets
import torchvision.transforms as T
import torch.nn.functional as F
import torchvision.models as models
import numpy as np
import copy

# 从训练集的50000个样本中，取49000个作为训练集，剩余1000个作为验证集
from tqdm import tqdm

NUM_TRAIN = 49000

USE_GPU = True
dtype = torch.float32
if USE_GPU and torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

# 数据预处理，减去cifar-10数据均值
transform_normal = T.Compose([
    T.ToTensor(),
    T.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010))
])
# 数据增强
transform_aug = T.Compose([
    T.RandomCrop(32, padding=4),
    T.RandomHorizontalFlip(),
    T.ToTensor(),
    T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
])

# 加载训练集
cifar10_train = datasets.CIFAR10('./dataset', train=True, download=True, transform=transform_normal)
loader_train = DataLoader(cifar10_train, batch_size=64, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN)))

# 加载验证集
cifar10_val = datasets.CIFAR10('./dataset', train=True, download=True, transform=transform_normal)
loader_val = DataLoader(cifar10_val, batch_size=64, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN, 50000)))

# 加载测试集
cifar10_test = datasets.CIFAR10('./dataset', train=False, download=True, transform=transform_normal)
loader_test = DataLoader(cifar10_test, batch_size=64)


# 验证模型在验证集或者测试集上的准确率
def check_accuracy(loader, model):
    if loader.dataset.train:
        print('Checking accuracy on validation set')
    else:
        print('Checking accuracy on test set')
    num_correct = 0
    num_samples = 0
    model.eval()  # set model to evaluation mode
    with torch.no_grad():
        for inputs, labels in loader:
            inputs = inputs.to(device=device, dtype=dtype)
            labels = labels.to(device=device, dtype=torch.long)
            predict_scores = model(inputs)
            _, preds = predict_scores.max(1)
            num_correct += (preds == labels).sum()
            num_samples += preds.size(0)
        acc = float(num_correct) / num_samples
        print('Got %d / %d correct (%.2f)' % (num_correct, num_samples, 100 * acc))
        return acc


def train_model(model, optimizer, epochs=1, scheduler=None):
    '''
    Parameters:
    - model: A Pytorch Module giving the model to train.
    - optimizer: An optimizer object we will use to train the model
    - epochs: A Python integer giving the number of epochs to train
    Returns: best model
    '''
    lossfun = nn.CrossEntropyLoss()
    best_model_wts = None
    best_acc = 0.0
    model = model.to(device=device)  # move the model parameters to CPU/GPU
    for e in range(epochs):
        with tqdm(
                iterable=loader_train,
                bar_format='{desc} {n_fmt:>4s}/{total_fmt:<4s} {percentage:3.0f}%|{bar}| {postfix}',
        ) as t:
            t.set_description_str(f"\33[36m【Epoch {e + 1:04d}】")
            for _, (x, y) in enumerate(loader_train):
                model.train()  # set model to training mode
                x = x.to(device, dtype=dtype)
                y = y.to(device, dtype=torch.long)
                scores = model(x)
                loss = lossfun(scores, y).to(device)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                t.set_postfix_str(f"train_loss={loss.item():.4f}")
                t.update()
            scheduler.step()
            print('Epoch %d, loss=%.4f' % (e, loss.item()))
            print('learnning rate:', optimizer.state_dict()['param_groups'][0]['lr'])
            acc = check_accuracy(loader_val, model)
            if acc > best_acc:
                best_model_wts = copy.deepcopy(model.state_dict())
                best_acc = acc
            print('best_acc:', best_acc)
    model.load_state_dict(best_model_wts)
    return model


class ResBlock(nn.Module):
    def __init__(self, inchannel, outchannel, stride=1):
        super().__init__()
        # 这里定义了残差块内连续的2个卷积层
        self.left = nn.Sequential(
            nn.Conv2d(inchannel, outchannel, kernel_size=3, stride=stride, padding=1, bias=False),
            nn.BatchNorm2d(outchannel),
            nn.ReLU(inplace=True),
            nn.Conv2d(outchannel, outchannel, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(outchannel)
        )
        self.shortcut = nn.Sequential()
        if stride != 1 or inchannel != outchannel:
            # shortcut，这里为了跟2个卷积层的结果结构一致，要做处理
            self.shortcut = nn.Sequential(
                nn.Conv2d(inchannel, outchannel, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(outchannel)
            )

    def forward(self, x):
        out = self.left(x)
        # 将2个卷积层的输出跟处理过的x相加，实现ResNet的基本结构
        out = out + self.shortcut(x)
        out = F.relu(out)

        return out

class ResNet18(nn.Module):
    def __init__(self, ResBlock, num_classes=10):
        super().__init__()
        self.inchannel = 64
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer1 = self.make_layer(ResBlock, 64, 2, stride=1)
        self.layer2 = self.make_layer(ResBlock, 128, 2, stride=2)
        self.layer3 = self.make_layer(ResBlock, 256, 2, stride=2)
        self.layer4 = self.make_layer(ResBlock, 512, 2, stride=2)
        self.fc = nn.Linear(512, num_classes)

    # 这个函数主要是用来，重复同一个残差块
    def make_layer(self, block, channels, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(block(self.inchannel, channels, stride))
            self.inchannel = channels
        return nn.Sequential(*layers)

    def forward(self, x):
        # 在这里，整个ResNet18的结构就很清晰了
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool2d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out


# 训练更多代数，并应用学习率衰减
learning_rate = 1e-2
resnet = ResNet18(ResBlock)
optimizer_resnet = optim.Adam(resnet.parameters(), lr=learning_rate, betas=(0.8, 0.9))
scheduler = lr_scheduler.StepLR(optimizer_resnet, step_size=10, gamma=0.15)
best_resnet = train_model(resnet, optimizer_resnet, 150, scheduler)
check_accuracy(loader_test, best_resnet)
